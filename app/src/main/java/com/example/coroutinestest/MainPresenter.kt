package com.example.coroutinestest

import com.example.coroutinestest.networking.Repository
import com.example.coroutinestest.networking.TodoException
import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.coroutines.CoroutineContext
import kotlin.random.Random

class MainPresenter : CoroutineScope {

    private var mView: MainContract.View? = null


    /**
     * [CoroutineScope] implementation
     * */
    override val coroutineContext: CoroutineContext
        get() = SupervisorJob() + Dispatchers.Main


    /**
     * [MainPresenter] functions
     * */
    fun bindView(view: MainContract.View) {
        mView = view
    }

    fun getTodo(id: Int) {
        launch {
            try {
                val response = Repository.getTodo(id)
                mView?.onTodoAvailable(response.title)
            } catch (error: TodoException) {
                mView?.onTodoError()
            } catch (genericError: Exception) {
                mView?.onTodoError()
            }
        }
    }

}