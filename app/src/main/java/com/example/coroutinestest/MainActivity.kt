package com.example.coroutinestest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainContract.View {

    private val presenter = MainPresenter()
    private var currentTodoIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.bindView(view = this)
        nextTodoButton.setOnClickListener {
            presenter.getTodo(id = ++currentTodoIndex)
        }
    }

    /**
     * [MainContract.View] implementation
     * */
    override fun onTodoAvailable(title: String) {
        Toast.makeText(this, "Here's your next todo: $title", Toast.LENGTH_SHORT).show()
    }

    override fun onTodoError() {
        Toast.makeText(this, "Oops, impossible to get the next todo.\nEnjoy your free time!", Toast.LENGTH_LONG).show()
    }
}