package com.example.coroutinestest

interface MainContract {
    interface View {
        fun onTodoAvailable(title: String)
        fun onTodoError()
    }
}