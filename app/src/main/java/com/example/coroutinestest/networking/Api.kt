package com.example.coroutinestest.networking

import retrofit2.http.GET
import retrofit2.http.Path

interface WebService {
    @GET("/todos/{id}")
    suspend fun getTodo(@Path("id") id: Int): Todo
}