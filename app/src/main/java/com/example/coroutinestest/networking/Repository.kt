package com.example.coroutinestest.networking

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object Repository {
    private var client = RetrofitClient.retrofit

    suspend fun getTodo(id: Int) = withContext(Dispatchers.IO) {
        try {
            client.getTodo(id)
        } catch (cause: Throwable) {
            throw TodoException("Unable to retrieve todo", cause)
        }
    }
}